ldd2rpm
=======

Simple utility to display a list of RPM packages that provide the shared libraries
linked to specified executable.

Main use case is porting C/C++ programs to other (RPM based) Linux platforms. It gives the
end user an *indication* of the RPMs required.

This helps when say porting from Fedora to Centos for example.

Basically saves a lot of time doing **rpm -qf /path/to/libfoo** etc for all the
shared libs appearing in **ldd** output.

Note: The library support on Linux includes:

    * static libraries
    * shared libraries with Dynamic Linking
    * shared libraries with Dynamic loading (dlopen etc)

This tool helps with **shared libraries with dynamic linking**.
Libraries dynamically loaded (DL) at runtime will not be  displayed.

Note: It will only provide package information for shared libraries installed as RPMs.

Requirements
------------
This tool uses the Python 3 **rpm** module. On Centos 7 its provided by **rpm-python**. It is similar for other
Redhat/Fedora/Centos releases. Fedora 31 provides this module from **python3-rpm** package.

Usage
-----

This is an example **ldd2rpm** output from a C++ game compiled on **Centos 7**

``` text
$ ./ldd2rpm.py ../missile_run/build/missile_run
systemd-libs-219-42.el7_4.10.x86_64
libXrender-0.9.10-1.el7.x86_64
freetype-2.4.11-15.el7.x86_64
libSM-1.2.2-2.el7.x86_64
zlib-1.2.7-17.el7.x86_64
libuuid-2.23.2-43.el7_4.2.x86_64
elfutils-libs-0.168-8.el7.x86_64
libxcb-1.12-1.el7.x86_64
libICE-1.0.9-9.el7.x86_64
libXau-1.0.8-2.1.el7.x86_64
xz-libs-5.2.2-1.el7.x86_64
flac-libs-1.3.0-5.el7_1.x86_64
libgcc-4.8.5-16.el7_4.2.x86_64
libvorbis-1:1.3.3-8.el7.x86_64
elfutils-libelf-0.168-8.el7.x86_64
openal-soft-1.16.0-3.el7.x86_64
libXrandr-1.5.1-2.el7.x86_64
glibc-2.17-196.el7_4.2.x86_64
libX11-1.6.5-1.el7.x86_64
libjpeg-turbo-1.2.90-5.el7.x86_64
libstdc++-4.8.5-16.el7_4.2.x86_64
libXcomposite-0.4.4-4.1.el7.x86_64
libXfixes-5.0.3-1.el7.x86_64
libattr-2.4.46-12.el7.x86_64
libXdamage-1.1.4-4.1.el7.x86_64
libXext-1.3.3-3.el7.x86_64
bzip2-libs-1.0.6-13.el7.x86_64
libogg-2:1.3.0-7.el7.x86_64
libcap-2.22-9.el7.x86_64

```

Here is the same command, but increased **verbosity** has been requested. The second column
displays the shared library/libraries required by the executable. The RPM can provide one or more
of the libraries dynamically linked to the executable.

``` text
$ ./ldd2rpm.py -v ../missile_run/build/missile_run
systemd-libs-219-42.el7_4.10.x86_64                   /lib64/libudev.so.1
libXrender-0.9.10-1.el7.x86_64                        /lib64/libXrender.so.1
freetype-2.4.11-15.el7.x86_64                         /lib64/libfreetype.so.6
libSM-1.2.2-2.el7.x86_64                              /lib64/libSM.so.6
zlib-1.2.7-17.el7.x86_64                              /lib64/libz.so.1
libuuid-2.23.2-43.el7_4.2.x86_64                      /lib64/libuuid.so.1
elfutils-libs-0.168-8.el7.x86_64                      /lib64/libdw.so.1
libxcb-1.12-1.el7.x86_64                              /lib64/libxcb.so.1
libICE-1.0.9-9.el7.x86_64                             /lib64/libICE.so.6
libXau-1.0.8-2.1.el7.x86_64                           /lib64/libXau.so.6
xz-libs-5.2.2-1.el7.x86_64                            /lib64/liblzma.so.5
flac-libs-1.3.0-5.el7_1.x86_64                        /lib64/libFLAC.so.8
libgcc-4.8.5-16.el7_4.2.x86_64                        /lib64/libgcc_s.so.1
libvorbis-1:1.3.3-8.el7.x86_64                        /lib64/libvorbisenc.so.2
                                                      /lib64/libvorbisfile.so.3
                                                      /lib64/libvorbis.so.0
elfutils-libelf-0.168-8.el7.x86_64                    /lib64/libelf.so.1
openal-soft-1.16.0-3.el7.x86_64                       /lib64/libopenal.so.1
libXrandr-1.5.1-2.el7.x86_64                          /lib64/libXrandr.so.2
glibc-2.17-196.el7_4.2.x86_64                         /lib64/libm.so.6
                                                      /lib64/libc.so.6
                                                      /lib64/libpthread.so.0
                                                      /lib64/librt.so.1
                                                      /lib64/libdl.so.2
libX11-1.6.5-1.el7.x86_64                             /lib64/libX11.so.6
libjpeg-turbo-1.2.90-5.el7.x86_64                     /lib64/libjpeg.so.62
libstdc++-4.8.5-16.el7_4.2.x86_64                     /lib64/libstdc++.so.6
libXcomposite-0.4.4-4.1.el7.x86_64                    /lib64/libXcomposite.so.1
libXfixes-5.0.3-1.el7.x86_64                          /lib64/libXfixes.so.3
libattr-2.4.46-12.el7.x86_64                          /lib64/libattr.so.1
libXdamage-1.1.4-4.1.el7.x86_64                       /lib64/libXdamage.so.1
libXext-1.3.3-3.el7.x86_64                            /lib64/libXext.so.6
bzip2-libs-1.0.6-13.el7.x86_64                        /lib64/libbz2.so.1
libogg-2:1.3.0-7.el7.x86_64                           /lib64/libogg.so.0
libcap-2.22-9.el7.x86_64                              /lib64/libcap.so.2


```

For reference here is the output from **ldd**. Note SFML libraries were not
installed as part of any RPM (compiled locally from source), so although they
appear in **ldd** output, they have no matching RPM.

``` text
$ ldd  ../missile_run/build/missile_run
	linux-vdso.so.1 =>  (0x00007fffc59ea000)
	libsfml-graphics.so.2.4 => /home/frank/sfml242/lib/libsfml-graphics.so.2.4 (0x00007f0cbee2d000)
	libsfml-window.so.2.4 => /home/frank/sfml242/lib/libsfml-window.so.2.4 (0x00007f0cbec07000)
	libsfml-system.so.2.4 => /home/frank/sfml242/lib/libsfml-system.so.2.4 (0x00007f0cbe9fa000)
	libsfml-audio.so.2.4 => /home/frank/sfml242/lib/libsfml-audio.so.2.4 (0x00007f0cbe7e0000)
	libstdc++.so.6 => /lib64/libstdc++.so.6 (0x00007f0cbe4ac000)
	libm.so.6 => /lib64/libm.so.6 (0x00007f0cbe1aa000)
	libgcc_s.so.1 => /lib64/libgcc_s.so.1 (0x00007f0cbdf94000)
	libc.so.6 => /lib64/libc.so.6 (0x00007f0cbdbd0000)
	libGL.so.1 => /var/lib/VBoxGuestAdditions/lib/libGL.so.1 (0x00007f0cbda45000)
	libSM.so.6 => /lib64/libSM.so.6 (0x00007f0cbd83d000)
	libICE.so.6 => /lib64/libICE.so.6 (0x00007f0cbd620000)
	libX11.so.6 => /lib64/libX11.so.6 (0x00007f0cbd2e2000)
	libXext.so.6 => /lib64/libXext.so.6 (0x00007f0cbd0d0000)
	libfreetype.so.6 => /lib64/libfreetype.so.6 (0x00007f0cbce29000)
	libjpeg.so.62 => /lib64/libjpeg.so.62 (0x00007f0cbcbd4000)
	libpthread.so.0 => /lib64/libpthread.so.0 (0x00007f0cbc9b8000)
	librt.so.1 => /lib64/librt.so.1 (0x00007f0cbc7af000)
	libXrandr.so.2 => /lib64/libXrandr.so.2 (0x00007f0cbc5a4000)
	libudev.so.1 => /lib64/libudev.so.1 (0x00007f0cbc58f000)
	libopenal.so.1 => /lib64/libopenal.so.1 (0x00007f0cbc31f000)
	libvorbisenc.so.2 => /lib64/libvorbisenc.so.2 (0x00007f0cbbe50000)
	libvorbisfile.so.3 => /lib64/libvorbisfile.so.3 (0x00007f0cbbc47000)
	libvorbis.so.0 => /lib64/libvorbis.so.0 (0x00007f0cbba18000)
	libogg.so.0 => /lib64/libogg.so.0 (0x00007f0cbb811000)
	libFLAC.so.8 => /lib64/libFLAC.so.8 (0x00007f0cbb5cc000)
	/lib64/ld-linux-x86-64.so.2 (0x000055a045070000)
	VBoxOGLcrutil.so => /lib64/VBoxOGLcrutil.so (0x00007f0cbb3dc000)
	libXcomposite.so.1 => /lib64/libXcomposite.so.1 (0x00007f0cbb1d9000)
	libXdamage.so.1 => /lib64/libXdamage.so.1 (0x00007f0cbafd6000)
	libXfixes.so.3 => /lib64/libXfixes.so.3 (0x00007f0cbadcf000)
	libuuid.so.1 => /lib64/libuuid.so.1 (0x00007f0cbabca000)
	libxcb.so.1 => /lib64/libxcb.so.1 (0x00007f0cba9a2000)
	libdl.so.2 => /lib64/libdl.so.2 (0x00007f0cba79d000)
	libXrender.so.1 => /lib64/libXrender.so.1 (0x00007f0cba592000)
	libcap.so.2 => /lib64/libcap.so.2 (0x00007f0cba38d000)
	libdw.so.1 => /lib64/libdw.so.1 (0x00007f0cba145000)
	libXau.so.6 => /lib64/libXau.so.6 (0x00007f0cb9f41000)
	libattr.so.1 => /lib64/libattr.so.1 (0x00007f0cb9d3b000)
	libelf.so.1 => /lib64/libelf.so.1 (0x00007f0cb9b23000)
	libz.so.1 => /lib64/libz.so.1 (0x00007f0cb990d000)
	liblzma.so.5 => /lib64/liblzma.so.5 (0x00007f0cb96e6000)
	libbz2.so.1 => /lib64/libbz2.so.1 (0x00007f0cb94d6000)

```

Here is an example of the same Game compiled on **Fedora 27**

``` text
10:06 $ ldd ../missile_run/build/missile_run
	linux-vdso.so.1 (0x00007ffe3bfa6000)
	libsfml-graphics.so.2.4 => /lib64/libsfml-graphics.so.2.4 (0x00007faeb3080000)
	libsfml-window.so.2.4 => /lib64/libsfml-window.so.2.4 (0x00007faeb2e61000)
	libsfml-system.so.2.4 => /lib64/libsfml-system.so.2.4 (0x00007faeb2c55000)
	libsfml-audio.so.2.4 => /lib64/libsfml-audio.so.2.4 (0x00007faeb2a3c000)
	libstdc++.so.6 => /lib64/libstdc++.so.6 (0x00007faeb26b5000)
	libm.so.6 => /lib64/libm.so.6 (0x00007faeb236a000)
	libgcc_s.so.1 => /lib64/libgcc_s.so.1 (0x00007faeb2153000)
	libc.so.6 => /lib64/libc.so.6 (0x00007faeb1d9d000)
	libGL.so.1 => /var/lib/VBoxGuestAdditions/lib/libGL.so.1 (0x00007faeb333c000)
	libX11.so.6 => /lib64/libX11.so.6 (0x00007faeb1a5f000)
	libXext.so.6 => /lib64/libXext.so.6 (0x00007faeb184d000)
	libfreetype.so.6 => /lib64/libfreetype.so.6 (0x00007faeb1598000)
	libjpeg.so.62 => /lib64/libjpeg.so.62 (0x00007faeb1330000)
	libpthread.so.0 => /lib64/libpthread.so.0 (0x00007faeb1112000)
	librt.so.1 => /lib64/librt.so.1 (0x00007faeb0f0a000)
	libXrandr.so.2 => /lib64/libXrandr.so.2 (0x00007faeb0cff000)
	libudev.so.1 => /lib64/libudev.so.1 (0x00007faeb0ae1000)
	libopenal.so.1 => /lib64/libopenal.so.1 (0x00007faeb0846000)
	libvorbisenc.so.2 => /lib64/libvorbisenc.so.2 (0x00007faeb059d000)
	libvorbisfile.so.3 => /lib64/libvorbisfile.so.3 (0x00007faeb0394000)
	libvorbis.so.0 => /lib64/libvorbis.so.0 (0x00007faeb0168000)
	libogg.so.0 => /lib64/libogg.so.0 (0x00007faeaff61000)
	libFLAC.so.8 => /lib64/libFLAC.so.8 (0x00007faeafd08000)
	/lib64/ld-linux-x86-64.so.2 (0x00007faeb32c2000)
	VBoxOGLcrutil.so => /lib64/VBoxOGLcrutil.so (0x00007faeafb19000)
	libXcomposite.so.1 => /lib64/libXcomposite.so.1 (0x00007faeaf916000)
	libXdamage.so.1 => /lib64/libXdamage.so.1 (0x00007faeaf713000)
	libXfixes.so.3 => /lib64/libXfixes.so.3 (0x00007faeaf50d000)
	libxcb.so.1 => /lib64/libxcb.so.1 (0x00007faeaf2e5000)
	libdl.so.2 => /lib64/libdl.so.2 (0x00007faeaf0e1000)
	libbz2.so.1 => /lib64/libbz2.so.1 (0x00007faeaeed0000)
	libpng16.so.16 => /lib64/libpng16.so.16 (0x00007faeaec9d000)
	libz.so.1 => /lib64/libz.so.1 (0x00007faeaea86000)
	libXrender.so.1 => /lib64/libXrender.so.1 (0x00007faeae87c000)
	libXau.so.6 => /lib64/libXau.so.6 (0x00007faeae678000)


```
Here is the corresponding  **ldd2rpm** output.

``` text
10:08 $ ./ldd2rpm.py  ../missile_run/build/missile_run
systemd-libs-234-10.git5f8984e.fc27.x86_64
glibc-2.26-27.fc27.x86_64
libstdc++-7.3.1-5.fc27.x86_64
libXrandr-1.5.1-4.fc27.x86_64
libXcomposite-0.4.4-11.fc27.x86_64
freetype-2.8-8.fc27.x86_64
libvorbis-1:1.3.6-1.fc27.x86_64
zlib-1.2.11-4.fc27.x86_64
libXfixes-5.0.3-4.fc27.x86_64
libpng-2:1.6.31-1.fc27.x86_64
libX11-1.6.5-4.fc27.x86_64
libgcc-7.3.1-5.fc27.x86_64
libjpeg-turbo-1.5.3-1.fc27.x86_64
bzip2-libs-1.0.6-24.fc27.x86_64
libogg-2:1.3.2-8.fc27.x86_64
libxcb-1.12-5.fc27.x86_64
openal-soft-1.18.2-5.fc27.x86_64
SFML-2.4.2-4.fc27.x86_64
libXau-1.0.8-9.fc27.x86_64
libXext-1.3.3-7.fc27.x86_64
libXrender-0.9.10-4.fc27.x86_64
flac-libs-1.3.2-4.fc27.x86_64
libXdamage-1.1.4-11.fc27.x86_64


```
Here is the **verbose** output.

``` text
10:08 $ ./ldd2rpm.py -v ../missile_run/build/missile_run
systemd-libs-234-10.git5f8984e.fc27.x86_64            /lib64/libudev.so.1
glibc-2.26-27.fc27.x86_64                             /lib64/libm.so.6
                                                      /lib64/libc.so.6
                                                      /lib64/libpthread.so.0
                                                      /lib64/librt.so.1
                                                      /lib64/libdl.so.2
libstdc++-7.3.1-5.fc27.x86_64                         /lib64/libstdc++.so.6
libXrandr-1.5.1-4.fc27.x86_64                         /lib64/libXrandr.so.2
libXcomposite-0.4.4-11.fc27.x86_64                    /lib64/libXcomposite.so.1
freetype-2.8-8.fc27.x86_64                            /lib64/libfreetype.so.6
libvorbis-1:1.3.6-1.fc27.x86_64                       /lib64/libvorbisenc.so.2
                                                      /lib64/libvorbisfile.so.3
                                                      /lib64/libvorbis.so.0
zlib-1.2.11-4.fc27.x86_64                             /lib64/libz.so.1
libXfixes-5.0.3-4.fc27.x86_64                         /lib64/libXfixes.so.3
libpng-2:1.6.31-1.fc27.x86_64                         /lib64/libpng16.so.16
libX11-1.6.5-4.fc27.x86_64                            /lib64/libX11.so.6
libgcc-7.3.1-5.fc27.x86_64                            /lib64/libgcc_s.so.1
libjpeg-turbo-1.5.3-1.fc27.x86_64                     /lib64/libjpeg.so.62
bzip2-libs-1.0.6-24.fc27.x86_64                       /lib64/libbz2.so.1
libogg-2:1.3.2-8.fc27.x86_64                          /lib64/libogg.so.0
libxcb-1.12-5.fc27.x86_64                             /lib64/libxcb.so.1
openal-soft-1.18.2-5.fc27.x86_64                      /lib64/libopenal.so.1
SFML-2.4.2-4.fc27.x86_64                              /lib64/libsfml-graphics.so.2.4
                                                      /lib64/libsfml-window.so.2.4
                                                      /lib64/libsfml-system.so.2.4
                                                      /lib64/libsfml-audio.so.2.4
libXau-1.0.8-9.fc27.x86_64                            /lib64/libXau.so.6
libXext-1.3.3-7.fc27.x86_64                           /lib64/libXext.so.6
libXrender-0.9.10-4.fc27.x86_64                       /lib64/libXrender.so.1
flac-libs-1.3.2-4.fc27.x86_64                         /lib64/libFLAC.so.8
libXdamage-1.1.4-11.fc27.x86_64                       /lib64/libXdamage.so.1

```

Note: As SFML was installed on **Fedora 27** from RPM, it appears in the output.

Enjoy !!
