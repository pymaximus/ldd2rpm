#!/usr/bin/env python
# coding: utf-8

#
# ldd2rpm - Create rpm package list from Linux executable.
#
# Copyright (C) 2018 - Frank Singleton
#
# Small utility to generate a list of RPM's that provide the required shared
# libraries for specified application. May not cover all cases (like
# dynamically loading a library etc), but can minimize some of the manual work.
#
# Usage: ldd2rpm.py [-v] <executable>
#
# Example:
#
# [frank@localhost ldd2rpm]$ ./ldd2rpm.py  ../missile_run/build/missile_run
# systemd-libs-219-42.el7_4.10.x86_64
# libXrender-0.9.10-1.el7.x86_64
# freetype-2.4.11-15.el7.x86_64
# libSM-1.2.2-2.el7.x86_64
# zlib-1.2.7-17.el7.x86_64
# libuuid-2.23.2-43.el7_4.2.x86_64
# elfutils-libs-0.168-8.el7.x86_64
# libxcb-1.12-1.el7.x86_64
# libICE-1.0.9-9.el7.x86_64
# libXau-1.0.8-2.1.el7.x86_64
# xz-libs-5.2.2-1.el7.x86_64
# flac-libs-1.3.0-5.el7_1.x86_64
# libgcc-4.8.5-16.el7_4.2.x86_64
# libvorbis-1:1.3.3-8.el7.x86_64
# elfutils-libelf-0.168-8.el7.x86_64
# openal-soft-1.16.0-3.el7.x86_64
# libXrandr-1.5.1-2.el7.x86_64
# glibc-2.17-196.el7_4.2.x86_64
# libX11-1.6.5-1.el7.x86_64
# libjpeg-turbo-1.2.90-5.el7.x86_64
# libstdc++-4.8.5-16.el7_4.2.x86_64
# libXcomposite-0.4.4-4.1.el7.x86_64
# libXfixes-5.0.3-1.el7.x86_64
# libattr-2.4.46-12.el7.x86_64
# libXdamage-1.1.4-4.1.el7.x86_64
# libXext-1.3.3-3.el7.x86_64
# bzip2-libs-1.0.6-13.el7.x86_64
# libogg-2:1.3.0-7.el7.x86_64
# libcap-2.22-9.el7.x86_64


# standard imports
import argparse
import rpm
import subprocess


def rpm_provides(filename, rpm_dict):
    """
    Populate rpm_dict with rpm providing shared library. Key is RPM, val is
    list of shared libraries.
    """
    ts = rpm.TransactionSet()
    headers = ts.dbMatch('basenames', filename)
    for h in headers:
        # print(h[rpm.RPMTAG_EVR])
        pkg = h[rpm.RPMTAG_NEVRA]
        # store as first entry in list, or append if already present.
        if pkg not in rpm_dict:
            rpm_dict[pkg] = [filename]
        else:
            rpm_dict[pkg].append(filename)

    return


def ldd(filename):
    "Execute ldd and return output]"
    output = subprocess.check_output(['ldd', filename])
    return output.decode('utf-8')


def ldd2list(filename):
    "Convert ldd output to a list of lines"
    output = ldd(filename)
    return output.splitlines()


def rpms_from_executable(filename):
    "Fetch list of RPMS lnked to executable "
    rpm_dict = {}
    ldd_list = ldd2list(filename)
    # parse ldd output lines, and find RPM's that provide the shared libraries
    for entry in ldd_list:
        if 'ld-linux' not in entry:
            if 'linux-vdso' not in entry:
                if 'linux-gate' not in entry:
                    lib_path = entry.split()[2]
                    rpm_provides(lib_path, rpm_dict)

    return rpm_dict


def do_main():
    # handle args
    parser = argparse.ArgumentParser(add_help=True)
    parser.add_argument('-v', action='store_true',
                        help='Request printing of shared libs')
    parser.add_argument('filename', action='store',
                        help='Filename (executable) to anlayze')

    args = parser.parse_args()

    rpms = rpms_from_executable(args.filename)
    # print nice output
    for k, v in rpms.items():
        if args.v:
            rpm_printed = False
            for t in v:
                if not rpm_printed:
                    print('%-50s    %s' % (k, t))
                    rpm_printed = True
                else:
                    print('%-50s    %s' % ("", t))
        else:
            print('%s' % k)


if __name__ == '__main__':
    do_main()
